<?php
	include "dbconnect.php";

?>

<!DOCTYPE html>
<html>

<head>
    <?php include "stuffs.php"; ?>

</head>
<style>
    body {
        font-family: sans-serif;
    }
</style>

<body>
    <div class="bgimg">
        <h3 class="dept-title">Department of Computer Science and Engineering</h3>

        <div class="middle">
            <form class="form" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" autocomplete="off">
                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" name="name" class="form-control" id="" required>
                </div>
                <div class="form-group">
                    <label>Reg No:</label>
                    <input type="number" name="regno" class="form-control" id="" required>
                </div>
                <div class="form-group">
                    <label>Year:</label>
                    <select class="form-control" name="year" required>
                        <option value=""></option>
                        <option value="FIRST YEAR">First Year</option>
                        <option value="SECOND YEAR">Second Year</option>
                        <option value="THIRD YEAR">Third Year</option>
                        <option value="FINAL YEAR">Final Year</option>
                    </select>
                </div><div class="form-group">
                    <label>Section:</label>
                    <select class="form-control" name="sec" required>
                        <option value=""></option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                
                    </select>
                </div>
                <input type="submit" value="Add Student" name="submit" class="btn btn-block ">
            </form>
<?php
if(isset($_POST["submit"])){
    $name=$_POST["name"];
    $regno=$_POST["regno"];
    $year=$_POST["year"];
    $sec=$_POST["sec"];
    $sql="SELECT * FROM student WHERE REGNO=$regno";
    $res=$db->query($sql);
		// echo $res->num_rows;
			if($res->num_rows>0)
			 {
				echo '<script>swal("Sorry!", "The Student  '.$name.'  already available", "warning");</script>';
			 }
			else{

                $sql = "INSERT INTO student ( NAME,REGNO,YEAR,SEC) VALUES ('$name','$regno','$year','$sec');";
                if($db->query($sql))
				{
				echo '<script>swal("Good Job..!","The Student '.$name.' added ..","success");</script>';
				}
				else
				{
				echo '<script>swal("Sorry !","Some Error Occured. Please try after some time","error");</script>';
				
				}
            }
}
?>
        </div>
    </div>
</body>

</html>