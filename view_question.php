<?php
	include "dbconnect.php";
session_start();
	if(!isset($_SESSION["AID"]))
	{
		header('Location:admin.php');
	}
?>

<!DOCTYPE html>
<html>

<head>
  <?php include "stuffs.php"; ?>

</head>
<style>
    body {
        font-family: sans-serif;
    }
</style>

<body>
    
        <h3 class="dept-title">Department of Computer Science and Engineering</h3>
        <div class="container">
            <div class="row fs">
        <div class="col-sm-1"></div>
        <div class="col-sm-10 ">
            <center>
        <div class="table-responsive">
        <table class="table table-stripped">
            <thead>
                <th>No. </th>
                <th>QUE </th>
                <th>OPTION A</th>
                <th>OPTION B </th>
                <th>OPTION C </th>
                <th>OPTION D </th>
                <th>ANS </th>
                <th>ROUND </th>
                <th>DELETE </th>
            </thead>
            <tbody>
               <?php
    $sql="SELECT * FROM question ORDER BY ROUNDS ASC";
      $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                            $i=1;
                            while($row=$res->fetch_assoc())
                        {
                            
                            $qid=$row["QID"];
                            $que=$row["QUESTION"];
                            $opA=$row["OPTIONA"];
                            $opB=$row["OPTIONB"];
                            $opC=$row["OPTIONC"];
                            $opD=$row["OPTIOND"];
                            $ans=$row["CORRECT"];
                            $round=$row["ROUNDS"];
                            echo "<tr>
                            <td>$i</td>
                            <td>$que</td>
                            <td>$opA </td>
                            <td>$opB</td>
                            <td>$opC</td>
                            <td>$opD</td>
                            <td>$ans</td>
                            <td>$round</td>
                           <td><a href='delQues.php?id=$qid' class='btn btn-warning'><span class='fa fa-trash'></span></a></td>
                            </tr>";
                            $i++;
                        }
                    }

                ?>
            </tbody>
           
        </table>
        </div>
        </center>
        </div>
        <div class="col-sm-1"></div>
        </div>
            </div>
             <br> <br> <br> <br> <br> <br>
</body>

</html>