<?php
	include "dbconnect.php";
session_start();
	if(!isset($_SESSION["AID"]))
	{
		header('Location:admin.php');
	}
?>

<!DOCTYPE html>
<html>

<head>
   <?php include "stuffs.php"; ?>

</head>
<style>
    body {
        font-family: sans-serif;
    }
</style>

<body>
    <div class="bgimg">
        <h3 class="dept-title">Department of Computer Science and Engineering</h3>
           <div class="container">
               <div class="row fs">
                   <div class="col-sm-4">
                       <div class="square">
                           <a href="add_students.php" title="add students">
                                <span class="fa fa-user-plus"></span>
                           </a>
                       </div>
                     </div>
                    <div class="col-sm-4">
                       <div class="square">
                           <a href="view_student.php" title="view students">
                                <span class="fa fa-eye"></span>
                           </a>
                       </div>
                   </div> 
                   <div class="col-sm-4">
                       <div class="square">
                           <a href="add_question.php" title="add questions">
                                <span class="fa fa-question-circle"></span>
                           </a>
                       </div>
                   </div>
                </div>
                <div class="row fs">
                    <div class="col-sm-4">
                       <div class="square">
                           <a href="view_question.php" title="view Questions and answers">
                                <span class="fa fa-book"></span>
                           </a>
                       </div>
                   </div>
                    <div class="col-sm-4">
                       <div class="square">
                           <a href="viewResult.php" tilte="view result">
                                <span class="fa fa-file"></span>
                           </a>
                       </div>
                   </div>
                    <div class="col-sm-4">
                       <div class="square">
                           <a href="logout.php" title="logout">
                                <span class="fa fa-power-off"></span>
                           </a>
                       </div>
                   </div>

               </div>
           </div>
    </div>
</body>

</html>