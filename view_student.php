<?php
	include "dbconnect.php";
session_start();
	if(!isset($_SESSION["AID"]))
	{
		header('Location:admin.php');
	}
?>

<!DOCTYPE html>
<html>

<head>
  <?php include "stuffs.php"; ?>

</head>
<style>
    body {
        font-family: sans-serif;
    }
</style>

<body>
    
        <h3 class="dept-title">Department of Computer Science and Engineering</h3>
        <div class="container">
            <div class="row fs">
        <div class="col-sm-2"></div>
        <div class="col-sm-8 ">
            <center>
        <div class="table-responsive">
        <table class="table table-stripped">
            <thead>
                <th>S.no </th>
                <th>Name </th>
                <th>Reg No </th>
                <th>Year </th>
                <th>Sec </th>
                <th>Round </th>
    
              <th>Delete </th>
            </thead>
            <tbody>
               <?php
    $sql="SELECT * FROM student ORDER BY NAME ASC";
      $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                            $i=1;
                            
                            while($row=$res->fetch_assoc())
                        {
                            $name=$row["NAME"];
                            $regno=$row["REGNO"];
                            $year=$row["YEAR"];
                            $sec=$row["SEC"];
                            $round=$row["ROUND"];
                            $sid=$row["ID"];
                            echo "<tr>
                            <td>$i</td>
                            <td>$name</td>
                            <td>$regno</td>
                            <td>$year</td>
                            <td>$sec</td>
                            <td>$round</td>
                            <td><a href='delStudent.php?id=$sid' class='btn btn-warning'><span class='fa fa-trash'></span></a></td>
                            </tr>";
                            $i++;
                        }
                    }

                ?>
            </tbody>
        </table>
        </div>
        </center>
        </div>
        <div class="col-sm-2"></div>
        </div>
            </div>
            <br>            <br>            <br>            <br>
</body>

</html>