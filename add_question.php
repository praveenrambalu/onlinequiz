<?php
	include "dbconnect.php";
session_start();
	if(!isset($_SESSION["AID"]))
	{
		header('Location:admin.php');
	}
?>

<!DOCTYPE html>
<html>

<head>
     <?php include "stuffs.php"; ?>

</head>
<style>
    body {
        font-family: sans-serif;
    }
</style>

<body>
    <div class="bgimg">
        <h3 class="dept-title">Department of Computer Science and Engineering</h3>
        <div class="container">
        <div class="row">
<div class="col-sm-3"></div>
<div class="col-sm-6">
      
            <form class="form" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>" autocomplete="off">
                <div class="form-group">
                    <label>Question:</label>
                    <input type="text" name="que" class="form-control" id="" required>
                </div>
                <div class="form-group">
                    <label>Option  A:</label>
                    <input type="text" name="opA" class="form-control" id="" required>
                </div>
                <div class="form-group">
                    <label>Option B:</label>
                    <input type="text" name="opB" class="form-control" id="" required>
                </div>
                <div class="form-group">
                    <label>Option C:</label>
                    <input type="text" name="opC" class="form-control" id="" required>
                </div>
                <div class="form-group">
                    <label>Option D:</label>
                    <input type="text" name="opD" class="form-control" id="" required>
                </div>
                <div class="form-group">
                    <label>Round:</label>
                    <input type="radio" name="round" value="TECH1">Technical round -1
                    <input type="radio" name="round" value="TECH2">Technical round -2
                    <input type="radio" name="round" value="WEB">Web round
                   
                </div>
                <div class="form-group">
                    <label>Answer:</label>
                    <select class="form-control" name="ans" required>
                        <option value=""></option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                
                    </select>
                </div>
                <input type="submit" value="Add Question" name="submit" class="btn btn-block ">
            </form>
<?php
if(isset($_POST["submit"])){
   $que=$_POST["que"];
   $opA=$_POST["opA"];
   $opB=$_POST["opB"];
   $opC=$_POST["opC"];
   $opD=$_POST["opD"];
   $round=$_POST["round"];
   $ans=$_POST["ans"];
  

                $sql = "INSERT INTO question(QUESTION, OPTIONA, OPTIONB, OPTIONC, OPTIOND, ROUNDS, CORRECT) VALUES('$que','$opA','$opB','$opC','$opD','$round','$ans');";
            //    echo $sql;
                if($db->query($sql))
				{
				echo '<script>swal("Good Job..!","The Question  added ..","success");</script>';
				}
				else
				{
				echo '<script>swal("Sorry !","Some Error Occured. Please try after some time","error");</script>';
				
				}
           
}
?>
   </div>
<div class="col-sm-3"></div> 
</div>  
</div>
    </div>
</body>

</html>