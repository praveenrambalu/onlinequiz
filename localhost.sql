-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 22, 2018 at 10:37 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id6435337_mitcse`
--
CREATE DATABASE IF NOT EXISTS `id6435337_mitcse` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `id6435337_mitcse`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `AID` int(11) NOT NULL,
  `AUSERNAME` varchar(30) NOT NULL,
  `APASSWORD` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`AID`, `AUSERNAME`, `APASSWORD`) VALUES
(1, 'praveenrambalu', 'a63208d97ee31c052f0802e19301936f '),
(2, 'subbu', 'e060371a4a3a5e6508290fcdfeff8d7f '),
(3, 'sivaraja', 'e060371a4a3a5e6508290fcdfeff8d7f '),
(4, 'bala', 'e060371a4a3a5e6508290fcdfeff8d7f ');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `QID` int(11) NOT NULL,
  `QUESTION` text NOT NULL,
  `OPTIONA` text NOT NULL,
  `OPTIONB` text NOT NULL,
  `OPTIONC` text NOT NULL,
  `OPTIOND` text NOT NULL,
  `ROUNDS` varchar(30) NOT NULL,
  `CORRECT` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`QID`, `QUESTION`, `OPTIONA`, `OPTIONB`, `OPTIONC`, `OPTIOND`, `ROUNDS`, `CORRECT`) VALUES
(1, 'Which choice is the correct HTML5 element for playing video files?', '&lt;video&gt;', '&lt;media&gt;', '&lt;movie&gt;', '&lt;mp4&gt;', 'TECH1', 'A'),
(2, 'The &lt;canvas&gt; element in HTML5 is used to:', 'display Excel records', 'manipulate data in a database', 'draw graphics', 'create draggable elements', 'TECH1', 'C'),
(3, 'Which tag contains the navigation?', '&lt;navi&gt;', '&lt;nav&gt;', '&lt;navigation&gt;', '&lt;div&gt;', 'TECH1', 'B'),
(4, 'Fill in the Blank <br> &lt;form&gt; <br> &lt;input ______=\"text\" name=\"name\" /&gt;<br> &lt;/form&gt;', 'text', 'textfield', 'value', 'type', 'TECH1', 'D'),
(5, 'From the three types of styling, which one is the most useful in terms of website optimization?', 'Inline', 'External', 'Internal', 'none of the above', 'TECH1', 'B'),
(6, 'What is the \"style\", when creating an internal CSS?', 'property', 'value', 'tag', 'attribute', 'TECH1', 'C'),
(7, 'Fill in the blank to apply white text color to the paragraph.p {______:#FFF;}', 'Background', 'Color', 'Background-color', 'text-color', 'TECH1', 'B'),
(8, 'How do you make a list not display bullet points?', 'list: none', 'list-style-type: none', 'bulletpoints: none', 'list-style-type: no-bullet', 'TECH1', 'B'),
(9, 'What does HTML stand for?', 'Home Tool Markup Language', 'Hyper Tension Markup language', 'Hyperlinks and Text Markup Language', 'Hyper Text Markup Language', 'TECH1', 'D'),
(10, 'Who is making the Web standards?', 'Mozilla', 'Godaddy', 'The World Wide Web Consortium(w3.org)', 'Google', 'TECH1', 'C'),
(11, 'Choose the correct HTML element for the largest heading:', '&lt;h6&gt;', '&lt;h1&gt;', '&lt;heading&gt;', '&lt;head&gt;', 'TECH1', 'B'),
(12, 'What is the correct HTML element for inserting a line break?', '&lt;lb&gt;', '&lt;break&gt;', '&lt;linebreak&gt;', '&lt;br&gt;', 'TECH1', 'D'),
(13, 'What is the correct HTML for adding a background color?', '&lt;body bg=\"yellow\"&gt;', '&lt;background&gt;yellow&lt;/background&gt;', '&lt;body style=\"background-color:yellow;\"&gt;', ' &lt;body class=\"yellow;\"&gt;', 'TECH1', 'C'),
(14, 'Choose the correct HTML element to define important text', '&lt; i &gt;', '&lt; important &gt;', '&lt; b &gt;', '&lt; strong &gt;', 'TECH1', 'D'),
(15, 'What is the correct HTML for creating a hyperlink?', '&lt;a url=\"http://www.w3schools.com\"&gt;W3Schools.com&lt;/a&gt;', '&lt;a href=\"http://www.w3schools.com\"&gt;W3Schools&lt;/a&gt;', ' &lt;a&gt;http://www.w3schools.com&lt;/a&gt;', ' &lt;a name=\"http://www.w3schools.com\"&gt;W3Schools.com&lt;/a&gt;', 'TECH1', 'B'),
(16, 'What does CSS stand for?', 'Cascading Style Sheets', ' Creative Style Sheets', 'Colorful Style Sheets', 'Computer Style Sheets', 'TECH1', 'A'),
(17, 'What is the correct HTML for referring to an external style sheet?', '&lt;link rel=\"stylesheet\" type=\"text/css\" href=\"mystyle.css\"&gt;', '&lt;stylesheet&gt;mystyle.css&lt;/stylesheet&gt;', ' &lt;style src=\"mystyle.css\"&gt;', 'all the above', 'TECH1', 'A'),
(18, 'Where in an HTML document is the correct place to refer to an external style sheet?', 'In the &lt;body&gt; section', 'At the end of the document', 'In the &lt;head&gt; section', 'All the above', 'TECH1', 'C'),
(19, 'Which HTML attribute is used to define inline styles?', 'style', 'font', 'class', 'styles', 'TECH1', 'A'),
(20, 'Which is the correct CSS syntax?', '{body:color=black;}', 'body:color=black;', '{body;color:black;}', ' body {color: black;}', 'TECH1', 'D'),
(21, 'Which character is used to indicate an end tag?', '<', '*', '^', '/', 'TECH2', 'D'),
(22, 'Which of these elements are all &lt;table&gt; elements?', '&lt;table&gt;&lt;head&gt;&lt;tfoot&gt;', '&lt;thead&gt;&lt;body&gt;&lt;tr&gt;', '&lt;table&gt;&lt;tr&gt;&lt;tt&gt;', '&lt;table&gt;&lt;tr&gt;&lt;td&gt;', 'TECH2', 'D'),
(23, ' How can you make a numbered list?', '&lt; ul &gt;', '&lt; ol &gt;', '&lt; list &gt;', '&lt; dl &gt;', 'TECH2', 'B'),
(24, 'How do you add a background color for all &lt;h1&gt; elements?', 'h1 {background-color:#FFFFFF;}', 'h1.all {background-color:#FFFFFF;}', 'all.h1 {background-color:#FFFFFF;}', 'none of the above', 'TECH2', 'A'),
(25, 'What is the correct HTML for making a drop-down list?', '&lt;input type=\"dropdown\"&gt;', ' &lt;list&gt;', '&lt;select&gt;', '&lt;input type=\"list\"&gt;', 'TECH2', 'C'),
(26, 'What is the correct HTML for inserting an image?', '&lt;img src=\"image.gif\" alt=\"MyImage\"&gt;', ' &lt;img alt=\"MyImage\"&gt;image.gif&lt;/img&gt;', '&lt;img href=\"image.gif\" alt=\"MyImage\"&gt;', '&lt;image src=\"image.gif\" alt=\"MyImage\"&gt;', 'TECH2', 'A'),
(27, 'Which HTML element defines the title of a document?', '&lt;meta&gt;', '&lt;head&gt;', '&lt;heading&gt;', '&lt;title&gt;', 'TECH2', 'D'),
(28, 'Which HTML attribute specifies an alternate text for an image, if the image cannot be displayed?', 'longdesc', 'title', 'alt', 'src', 'TECH2', 'C'),
(29, 'Which CSS property controls the text size?', 'font-size', 'font-style', 'text-style', 'text-size', 'TECH2', 'A'),
(30, 'What is the correct CSS syntax for making all the &lt;p&gt; elements bold?', '&lt;p style=\"text-size:bold;\"&gt;', 'p {font-weight:bold;}', 'p {text-size:bold;}', '&lt;p style=\"font-size:bold;\"&gt;', 'TECH2', 'B'),
(31, 'How do you display hyperlinks without an underline?', 'a {text-decoration:none;}', 'a {underline:none;}', 'a {text-decoration:no-underline;}', 'a {decoration:no-underline;}', 'TECH2', 'A'),
(32, 'How do you display a border like this: <br> The top border = 10 pixels<br> The bottom border = 5 pixels<br> The left border = 20 pixels<br> The right border = 1pixel ?', 'border-width:10px 20px 5px 1px;', 'border-width:10px 1px 5px 20px;', 'border-width:10px 5px 20px 1px;', 'border-width:5px 20px 10px 1px;', 'TECH2', 'B'),
(33, 'How do you select an element with id \"demo\"?', ' .demo', ' demo', '#demo', '*demo', 'TECH2', 'C'),
(34, 'In HTML, which attribute is used to specify that an input field must be filled out?', 'placeholder', 'validate', 'formvalidate', 'required', 'TECH2', 'D'),
(35, 'In the following code snippet, what value is given for the left margin: margin: 5px 10px 3px 8px;', '3px', '10px', '8px', '5px', 'TECH2', 'C'),
(36, 'Which of the following would be used to create an ID called header which has a width of 750px, a height of 30px and the color of the text is black?', '#header { height: 30px; width: 750px; color: black; }', '.header { height: 30px; width: 750px; colour: black; }', '#header { height: 30px; width: 750px; text: black; }', '.header { height: 30px; width: 750px; color: black; }', 'TECH2', 'A'),
(37, 'What is the correct CSS syntax to change the font name?', 'font-name:', 'font:', 'font-family:', 'fontname:', 'TECH2', 'C'),
(38, 'A declaration is terminated by?', '. - period', '! - exclamation mark', ': - colon', '; - semi colon', 'TECH2', 'D'),
(39, 'HTML is?', 'Server Side Language', 'Client Side Language', 'Both', 'none of the above', 'TECH2', 'B'),
(40, 'what is the color of Blue ?', '#00ff00', 'rgba(255,255,255,80)', 'hsl(140,025,124)', 'rgb(0,0,255)', 'TECH2', 'D');

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `RID` int(11) NOT NULL,
  `ROLLNO` varchar(15) NOT NULL,
  `TECH1` int(25) DEFAULT NULL,
  `TECH2` int(25) DEFAULT NULL,
  `WEB` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`RID`, `ROLLNO`, `TECH1`, `TECH2`, `WEB`) VALUES
(1, '611616104076', 7, NULL, NULL),
(2, '611616104048', 16, 17, NULL),
(3, '611616104009', 14, 10, NULL),
(4, '611616104083', 14, 9, NULL),
(5, '611616104068', 14, 9, NULL),
(6, '611615104092', 16, 15, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `REGNO` varchar(12) NOT NULL,
  `YEAR` varchar(30) NOT NULL,
  `SEC` varchar(3) NOT NULL,
  `ROUND` varchar(30) NOT NULL DEFAULT 'TECH1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`ID`, `NAME`, `REGNO`, `YEAR`, `SEC`, `ROUND`) VALUES
(2, 'PRAVEENRAM', '611615104066', 'FINAL YEAR', 'B', 'TECH2'),
(3, 'KIRUTHIGA K', '611616104048', 'THIRD YEAR', 'A', 'TECH2'),
(4, 'GEETHA S', '611616104029', 'THIRD YEAR', 'A', 'TECH1'),
(5, 'PRAKASH S', '611616104080', 'THIRD YEAR', 'B', 'TECH1'),
(6, 'ATHITHI S', '611616104015', 'THIRD YEAR', 'A', 'TECH1'),
(7, 'ABINAYA S', '611616104001', 'THIRD YEAR', 'A', 'TECH1'),
(8, 'KALAISELVI R.S', '611616104042', 'THIRD YEAR', 'A', 'TECH2'),
(9, 'ASWATH S', '611616104014', 'THIRD YEAR', 'A', 'TECH2'),
(10, 'NANDHAKUMAR T', '611616104305', 'THIRD YEAR', 'B', 'TECH1'),
(12, 'THANESH K', '611616104103', 'THIRD YEAR', 'B', 'TECH1'),
(13, 'KIRUBA P', '611616104047', 'THIRD YEAR', 'A', 'TECH1'),
(14, 'PALANISAMY M', '611616104076', 'THIRD YEAR', 'B', 'TECH2'),
(15, 'SANJAIBAL D', '611616104093', 'THIRD YEAR', 'B', 'TECH1'),
(16, 'A.MAGESHWARAN', '611616104056', 'THIRD YEAR', 'A', 'TECH1'),
(17, 'RAJAMANICKAM.B', '611616104083', 'THIRD YEAR', 'B', 'TECH2'),
(18, 'M.J.ARUL PANDIAN', '611616104009', 'THIRD YEAR', 'A', 'TECH2'),
(19, 'AS.THIRUPATHI', '611616104105', 'THIRD YEAR', 'B', 'TECH1'),
(20, 'R.MEYYALAZHAN', '611616104062', 'THIRD YEAR', 'B', 'TECH1'),
(22, 'Devaraj G', '611616104020', 'THIRD YEAR', 'A', 'TECH1'),
(23, 'Muthu Kumar.s', '611616104069', 'THIRD YEAR', 'B', 'TECH1'),
(25, 'M.Vani', '611616104110', 'THIRD YEAR', 'B', 'TECH1'),
(26, 'S.santhiya', '611616104094', 'THIRD YEAR', 'B', 'TECH1'),
(27, 'MUTHU KAMATCHI.S', '611616104068', 'THIRD YEAR', 'B', 'TECH2'),
(28, 'SIVARAJA M', '611615104092', 'FINAL YEAR', 'B', 'TECH2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`AID`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`QID`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`RID`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `AID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `QID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `RID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
