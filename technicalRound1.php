<?php
	include "dbconnect.php";
session_start();
	if(!isset($_SESSION["REGNO"]))
	{
		header('Location:index.php');
    }
    
    else{
        $reg=$_SESSION['REGNO'];
        $sql="SELECT * FROM student WHERE REGNO=$reg";
          $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                             while($row=$res->fetch_assoc())
                        {
                          $sid=$row["ID"];
                          $name=$row["NAME"];
                          $regno=$row["REGNO"];
                          $round=$row["ROUND"];
                    //       $test=$row["TEST1"];
                    //         if ($test=="COM") {
                    // header("Location:index.php?war=you have completed the test 1. if you don't please contact the admin immediately");
                                
                    //         }
                        }

                        // $sql="UPDATE student SET TEST1 = 'COM' WHERE student.REGNO = $regno;";
                        // $db->query($sql);
                        
                    }

                      if($round!='TECH1'){
                    header("Location:index.php?war=Test not available for you please contact the admin");
     
           }

            $sql="SELECT * FROM result WHERE ROLLNO=$regno";
                        $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                             while($row=$res->fetch_assoc())
                        {
                            $TECH1=$row["TECH1"];
                        }
                        if ($TECH1!=NULL) {
                    header("Location:index.php?war=You have completed the Test1. any doubt contact the admin");
                            # code...
                        }
                    }
    }
?>
<!DOCTYPE html>
<html>

<head>
   <?php include "stuffs.php"; ?>

</head>
<style>
    body {
        font-family: sans-serif;
    }
    </style>

<body>

    <h3 class="dept-title">Department of Computer Science and Engineering</h3>
    <h4 class="text-center round-title">Technical Question - Round 1</h4>
    <h5 class="text-center">Welcome <?php echo "<span class='text-uppercase'>$name - </span> ( $regno )"?></h5>

    <div class="times"></div>


    <div class="container">
        <div class="row fs">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">

                <form class="form" action="" name="round" method="POST" id="round">
                    <?php
                    $sql="SELECT * FROM question WHERE ROUNDS='TECH1' LIMIT 0,20";
                       $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                            $i=1;
                             while($row=$res->fetch_assoc())
                        {
                            $que=$row["QUESTION"];
                            $opa=$row["OPTIONA"];
                            $opb=$row["OPTIONB"];
                            $opc=$row["OPTIONC"];
                            $opd=$row["OPTIOND"];
                            $qid=$row["QID"];
                            $ans1=$row["CORRECT"];
                   echo" <div class='form-group'>
                         <div class='qs'>$i.  $que</div>
                        <div class='ans'>
                            
                            <br>
                            <input type='radio' name='op$i' value='A' id=''>$opa<br>
                            <input type='radio' name='op$i' value='B' id=''>$opb<br>
                            <input type='radio' name='op$i' value='C' id=''>$opc<br>
                            <input type='radio' name='op$i' value='D' id=''>$opd
                        </div>
                    </div>";
                    $i++;
                         }
                    }

                    ?>
                    <input type="submit" name="submit" value="Submit" class="btn btn-info ansSub">
                    <br><br><br>
                </form>

<?php
if (isset($_POST["submit"])) {
 
    $sql="SELECT * FROM question WHERE ROUNDS='TECH1' LIMIT 0,20";
    $op=1;
                       $res=$db->query($sql);
                if($res->num_rows>0)
                        {
                            $ans=0;
                             while($row=$res->fetch_assoc())
                        {
                             $crct=$row["CORRECT"];
                            //  ECHO $crct; 
                             if ($_POST["op$op"]==$crct) {
                                $ans++;
                             }  
                             $op++;              
                        }



                            $sql="SELECT * FROM result WHERE ROLLNO='$regno';";
                          $res=$db->query($sql);
		// echo $res->num_rows;
                            if($res->num_rows>0)
                            {
                                echo '<script>swal("Sorry!", "The Student has completed  the Test 1", "warning");</script>';
                               
                            }
                            else{
                                
                                  $sql = "INSERT INTO result (ROLLNO,TECH1) VALUES ('$regno', '$ans')";
                                    if($db->query($sql))
                                {
                                echo '<script>swal("Good Job..!","The Round1 completed.. wait for the result ..","success");</script>';
                                }
                                else
                                {
                                echo '<script>swal("Sorry !","Some Error Occured. Please try after some time","error");</script>';
                                
                                }
                             }
                


                      
                    }
      
   
}
?>
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>
</body>
<script>
$(document).ready(function(){
    countdown();
function countdown() {
    

     var a=600000;
var myVar= setInterval(function(){ 
     a=a-1000;
      var minutes = Math.floor((a % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((a % (1000 * 60 * 60)) / 1000);
    $(".times").html("<b>"+minutes+"</b> m  <b>"+seconds+"</b> s");
    if(a==0)
    {
       swal("Time Out..!","Timeout of your Test Please Contact Praveenram","warning");
       clearInterval(myVar);
       setTimeout(() => {
           window.open('index.php?war="Timed Out of your first test Please contact the Admin"','_self')
       }, 2000);
    }
  }, 1000);
}
});
</script>
</html>